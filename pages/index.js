import Head from "next/head";
import React, { useState, useEffect } from "react";
import useSwr from "swr";
import "isomorphic-fetch";
import Music from "../components/Music";

export default function Home(props) {
  const [count, setCount] = useState(5);
  //const [format, setFormat] = useState("");
  const [unit, setUnit] = useState("paragraphs");
  const [content, setContent] = useState("");

  async function getLoremContentFromAPI() {
    const initialParams = {
      count: count,
      unit: unit,
    };

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json", // we will be sending JSON
      },
      body: JSON.stringify(initialParams),
    };

    const result = await fetch("/api/content", options).then((r) => r.text());

    return result;
  }

  async function loadGeneratedContent(content) {
    if (content != "") {
      var resultContainer = document.getElementById("result");
      resultContainer.innerHTML = content;

      if (!resultContainer.classList.contains("show-result")) {
        resultContainer.classList.add("show-result");
      }
    }
  }

  useEffect(() => {
    loadGeneratedContent(content);
  }, [content]);

  async function handleSumbit(ev) {
    ev.preventDefault();

    setContent(await getLoremContentFromAPI());

    // setContent(document.getElementById("result").textContent);
    // copyToClipboard(content);

    document.querySelector(".result").scrollIntoView({
      behavior: "smooth",
    });
  }

  function changeOption(ev) {
    setUnit(ev.target.value);
  }

  function copyToClipboard(text) {
    const listener = function (ev) {
      ev.preventDefault();
      ev.clipboardData.setData("text/plain", text);
      ev.clipboardData.setData("text/plain", text);
    };
    document.addEventListener("copy", listener);
    document.execCommand("copy");
    document.removeEventListener("copy", listener);
  }

  return (
    <div className="App">
      <Head>
        <title>Loremgen - Lorem Ipsum Generator</title>
        <meta
          name="description"
          content="Loremgen - An awesome and fast Lorem Ipsum Generator"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* <Music /> */}

      <div className="title-continer">
        <div>Just another</div>
        <h1>Lorem Ipsum Generetor</h1>
      </div>

      <div className="content">
        <form onSubmit={handleSumbit}>
          <label> Do you want... </label>
          <div className="container-input">
            <div className="check-option">
              <input
                type="radio"
                name="unit"
                id="unitP"
                value="paragraphs"
                onChange={(ev) => changeOption(ev)}
                checked={unit === "paragraphs" ? true : false}
              />
              <div className="option-bg"></div>
              <label htmlFor="unitP">Paragraphs</label>
            </div>
            <div className="check-option">
              <input
                type="radio"
                name="unit"
                id="unitS"
                value="sentences"
                onChange={(ev) => changeOption(ev)}
                checked={unit === "sentences" ? true : false}
              />
              <div className="option-bg"></div>
              <label htmlFor="unitS">Sentences</label>
            </div>

            <div className="check-option">
              <input
                type="radio"
                name="unit"
                id="unitW"
                value="words"
                onChange={(ev) => changeOption(ev)}
                checked={unit === "words" ? true : false}
              />
              <div className="option-bg"></div>
              <label htmlFor="unitW">Words</label>
            </div>
          </div>

          <label htmlFor="count">How many? </label>
          <div className="container-input">
            <input
              type="number"
              name="count"
              id="count"
              value={count}
              placeholder="Count"
              min="1"
              onChange={(event) => setCount(event.target.value)}
            />
          </div>

          <div className="container-input">
            <a href="#result">
              <button type="submit">
                Generate {count} {unit}
              </button>
            </a>
          </div>
        </form>

        <div className="result" id="result"></div>
      </div>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        html,
        body,
        body > div:first-child {
          min-height: 100vh;
        }

        * {
          box-sizing: border-box;
          text-decoration: none;
        }

        @import url("https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap");

        @font-face {
          font-family: Road Rage;
          src: url("assets/fonts/Road_Rage.otf");
          font-display: swap;
        }

        * {
          box-sizing: border-box;
        }

        body {
          /* background: url("http://api.thumbr.it/whitenoise-361x370.png?background=15424fff&noise=166f9e&density=100&opacity=31"); */
          background: url("assets/images/bg.webp");
          background-size: cover;
          background-position: center;
          text-align: center;
        }

        html,
        body {
          width: 100%;
          background-color: #282828;
          font-family: "Press Start 2P";
        }

        .App-logo {
          height: 40vmin;
          pointer-events: none;
        }

        .title-continer {
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: flex-start;
        }

        @media (prefers-reduced-motion: no-preference) {
          .App-logo {
            -webkit-animation: App-logo-spin infinite 20s linear;
            animation: App-logo-spin infinite 20s linear;
          }
        }

        .App-link {
          color: #61dafb;
        }

        .container-input {
          width: 100%;
          display: block;
          clear: both;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .App {
          width: 100%;
          min-height: 100vh;
          display: flex;
          align-items: center;
          justify-content: center;
          flex-direction: column;
          padding: 3em 15px;
        }

        .content {
          width: 100%;
          height: 100%;
          max-width: 1200px;
        }

        .content form {
          width: 100%;
          max-width: 400px;
          display: flex;
          flex-direction: column;
          margin: auto;
        }

        h1,
        h2,
        h3,
        h4 {
          width: 100%;
          display: block;
          margin: 0;
        }

        h1 {
          font-family: Road Rage;
          font-size: 48px;
          /* background: url("http://api.thumbr.it/whitenoise-361x370.png?background=ff447c84&noise=166f9e&density=100&opacity=100"); */
          background: -webkit-linear-gradient(#ff447d, #f96da7);
          -webkit-background-clip: text;
          -webkit-text-fill-color: transparent;
          padding: 0 10px;

          -webkit-filter: saturate(0.8) contrast(1.2) brightness(1);
          filter: saturate(0.8) contrast(1.2) brightness(1);
          line-height: 1.1em;
          margin: 0.3em 0;
          background-position: center;
        }

        @-webkit-keyframes animate {
          0% {
            transform: scale(1);
          }
          25% {
            transform: scale(1.1);
          }
          100% {
            transform: scale(1);
          }
        }

        @keyframes animate {
          0% {
            transform: scale(1);
          }
          25% {
            transform: scale(1.1);
          }
          100% {
            transform: scale(1);
          }
        }

        .title-continer div {
          color: #b4f2e1;
          font-size: 16px;
          font-family: "Press Start 2P", sans-serif;
        }

        form button[type="submit"] {
          width: 300px;
          padding: 15px;
          border: none;
          -moz-appearance: none;
          appearance: none;
          -webkit-appearance: none;
          margin: 15px auto 0;
          /* border: 1px solid #b4f2e1; */
          color: #b4f2e1;
          font-family: "Press Start 2P", sans-serif;
          position: relative;
          z-index: 2;
          background: -webkit-linear-gradient(#ff447d, #e42d79);
          font-size: 12px;
          transition: all 0.2s ease;
          cursor: pointer;
          display: block;
          margin-top: 25px;
          text-decoration: none;
        }
        form button[type="submit"]:hover,
        form button[type="submit"]:focus,
        form button[type="submit"]:active {
          background: #b4f2e1;
          /* border: 1px solid #b4f2e1; */
          color: #000;
        }
        form button[type="submit"]:hover:before {
          opacity: 1;
          content: "";
          position: absolute;
          left: 2px;
          top: 2px;
          border: 1px solid #e42d79;
          display: block;
          width: 100%;
          height: 100%;
          z-index: 1;
        }

        form button[type="submit"]:before {
          opacity: 1;
          content: "";
          position: absolute;
          left: 2px;
          top: 2px;
          border: 1px solid #b4f2e1;
          display: block;
          width: 100%;
          height: 100%;
          z-index: 1;
        }

        .result {
          background-color: #d5f2ea;
          margin: 35px auto;
          width: 100%;
          padding: 10px 25px;
          max-width: 800px;
          font-family: Arial;
          opacity: 0;
          transition: all 0.2s ease;
        }

        .show-result {
          opacity: 1;
        }

        .result p {
          text-align: justify;
          line-height: 1.4em;
          margin: 1em 0;
          text-align: justify;
          text-rendering: optimizeLegibility;
          text-indent: 2em;
        }

        input[type="radio"] {
          opacity: 0;
          width: 100%;
          color: #fff;
          position: absolute;
          top: 0;
          left: 0;
          cursor: pointer;
        }

        .check-option {
          position: relative;
          background: #2e032b;
          display: flex;
          justify-content: center;
          align-items: center;
          margin-top: 1em;
        }

        .check-option label {
          padding: 15px 20px;
          position: relative;
          z-index: 3;
          width: 100%;
          height: auto;
          display: block;
          text-align: center;
          color: #fff;
          cursor: pointer;
        }

        input,
        form > label {
          font-family: "Press Start 2P";
          font-size: 13px;
          color: #b4f2e1;
          display: block;
          margin-top: 1em;
        }

        form > label {
          margin-top: 1em;
        }

        .check-option > label {
          font-size: 11px;
        }

        input[type="number"] {
          background-color: #2e032b;
          border: none;
          box-shadow: 0 0 0px#f96da772, 0 0 0px #f96da772, 0 0 2px #f96da772,
            0 0 3px #ff447cc9, 0 0 4px;
          color: #fff;
          padding: 12px;
          width: 110px;
          font-family: "Press Start 2P";
          font-size: 12px;
        }

        input[type="number"] ::-moz-placeholder {
          /* Chrome, Firefox, Opera, Safari 10.1+ */
          color: #fff;
          opacity: 1; /* Firefox */
        }

        input[type="number"] :-ms-input-placeholder {
          /* Chrome, Firefox, Opera, Safari 10.1+ */
          color: #fff;
          opacity: 1; /* Firefox */
        }

        ::-webkit-input-placeholder {
          color: #000;
          opacity: 1;
        }

        ::-moz-placeholder {
          color: #fff;
        }

        :-ms-input-placeholder {
          color: #fff;
        }

        ::-ms-input-placeholder {
          color: #fff;
        }

        ::placeholder {
          color: #fff;
        }

        input[type="number"] ::-webkit-input-placeholder {
          /* Chrome, Firefox, Opera, Safari 10.1+ */
          color: #fff;
          opacity: 1; /* Firefox */
        }

        input[type="number"] ::-moz-placeholder {
          /* Chrome, Firefox, Opera, Safari 10.1+ */
          color: #fff;
          opacity: 1; /* Firefox */
        }

        input[type="number"] :-ms-input-placeholder {
          /* Chrome, Firefox, Opera, Safari 10.1+ */
          color: #fff;
          opacity: 1; /* Firefox */
        }

        input[type="number"] ::-ms-input-placeholder {
          /* Chrome, Firefox, Opera, Safari 10.1+ */
          color: #fff;
          opacity: 1; /* Firefox */
        }

        input[type="number"] ::placeholder {
          /* Chrome, Firefox, Opera, Safari 10.1+ */
          color: #fff;
          opacity: 1; /* Firefox */
        }

        input[type="number"] ::-ms-input-placeholder {
          /* Microsoft Edge */
          color: #fff;
        }

        input[type="radio"]:checked + .option-bg,
        input[type="radio"]:hover + .option-bg {
          width: 100%;
          height: 100%;
          z-index: 1;
          background: -webkit-linear-gradient(#ff447d, #e42d79);
          /* box-shadow: 0 0 0px #f96da772, 0 0 0px #f96da772, 0 0 2px #f96da772,
    0 0 3px #ff447cc9, 0 0 4px #ff447cc9, 0 0 5px #ff447cc9, 0 0 5px #ff447cc9,
    0 0 6px #ff447cc9; */
          position: absolute;
          left: 0;
          top: 0;
          -webkit-filter: saturate(0.8) contrast(1.2);
          filter: saturate(0.8) contrast(1.2);
          transition: all 0.2s ease-out;
        }

        @-webkit-keyframes App-logo-spin {
          from {
            transform: rotate(0deg);
          }
          to {
            transform: rotate(360deg);
          }
        }

        @keyframes App-logo-spin {
          from {
            transform: rotate(0deg);
          }
          to {
            transform: rotate(360deg);
          }
        }

        @media (max-width: 768px) {
          .title-continer div {
            font-size: 14px;
          }

          label {
            font-size: 12px;
          }
        }
      `}</style>
    </div>
  );
}
