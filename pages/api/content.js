const loremIpsum = require("lorem-ipsum").loremIpsum;

export default (req, res) => {
  const { count } = req.body;
  const { unit } = req.body;

  var paragraphs = loremIpsum({
    count: parseInt(count, 10), // Number of "words", "sentences", or "paragraphs"
    format: "html", // "plain" or "html"
    paragraphLowerBound: 3, // Min. number of sentences per paragraph.
    paragraphUpperBound: 7, // Max. number of sentences per paragarph.
    sentenceLowerBound: 5, // Min. number of words per sentence.
    sentenceUpperBound: 15, // Max. number of words per sentence.
    // suffix: "\n", // Line ending, defaults to "\n" or "\r\n" (win32)
    units: unit, // paragraph(s), "sentence(s)", or "word(s)"
  });

  return res.json(paragraphs);
};
