import React from "react";
import ReactAudioPlayer from "react-audio-player";
import "./Music.module.css";

function Music() {
  return (
    <ReactAudioPlayer
      src="assets/audio/lorem-soundtrack.mp3"
      volume={0.2}
      autoplay="true"
      controls
    />
  );
}

export default Music;
